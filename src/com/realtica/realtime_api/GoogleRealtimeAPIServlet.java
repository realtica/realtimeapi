package com.realtica.realtime_api;
import java.io.IOException;
import javax.servlet.http.*;

public class GoogleRealtimeAPIServlet extends HttpServlet {
	/**
	 * 
	 */
    private static final long serialVersionUID = -4079996831041325329L;

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("text/plain");
		resp.getWriter().println("Hello, world");
	}
}
