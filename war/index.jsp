<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%-- <%@ page import="java.io.PrintWriter"%> --%>
<%
// PrintWriter out =response.getWriter();
//response.addHeader( "X-FRAME-OPTIONS", "GOFORIT" );
//out.println("hmm");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=9">
  <meta http-equiv="X-UA-Compatible" content="chrome=1" />
  <meta name="description"
    content="The Google Drive Realtime API Playground allows you to experiment with the Google Drive Realtime API." />
  <meta name="keywords"
    content="OAuth, Google, realtime, POST, GET, PUT, token, client id, Javascript, operational, transforms" />
  <title>Google Drive Realtime API</title>

  <link rel="stylesheet" href="//developers.google.com/_static/css/screen.css" type="text/css" media="screen" charset="utf-8">
  <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/base/jquery-ui.css" />
  <link rel="stylesheet" href="css/main.css" type="text/css" media="screen" charset="utf-8">

  <script type="text/javascript" src="//www.google.com/jsapi"></script>
  <script type="text/javascript" src="//apis.google.com/js/api.js"></script>
  <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
  <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
  <script type="text/javascript" src="js/realtime-client-utils.js"></script>
  <script type="text/javascript" src="js/rtpg.js"></script>
  <script type="text/javascript" src="js/rtpg.log.js"></script>
  <script type="text/javascript" src="js/rtpg.string.js"></script>
  <script type="text/javascript" src="js/rtpg.list.js"></script>
  <script type="text/javascript" src="js/rtpg.map.js"></script>
  <script type="text/javascript" src="js/rtpg.custom.js"></script>
  <script type="text/javascript" src="js/rtpg.collaborators.js"></script>
  <script type="text/javascript" src="js/rtpg.ui.js"></script>
  
  <script src="js/codemirror.js"></script>
  <script src="js/htmlmixed.js"></script>
  <script src="js/xml.js"></script>
<link rel="stylesheet" href="css/codemirror.css">

</head>


<body>
  <div id="message"></div>

  
  <div class="rp-appbar">
    <h2 class="rp-appname">Google Drive Realtime API</h2>
    <div id="collaborators">
    </div>
  </div>

  <div id="leftContainer" style="border-right: 1px solid #ebebeb; margin-right: -1px;">

    <section id="demoUnauthorizedOverlay" >
      <div class="notesBox rp-greyRuledBottom">
        <button id="demoAuthorizeButton" disabled="true" class="rp-button rp-button-submit">Autorizar</button>
        <button id="createNewDoc" class="rp-button rp-button-red">Create</button>
        <button id="openExistingDoc" class="rp-button rp-button-submit">Open</button>
        <button id="demoShareButton" class="rp-button rp-button-share">Compartir</button>
      </div>
      <div class="rp-section-content">
        <div id="authorizedMessage" style="display:none; padding-top: 10px;">
          <span style="font-size: 20px; color: green;">&#10003;</span>
          La aplicacion ya puede usar Google Drive Realtime API
        </div>
      </div>  
        <div id="documentNameContainer" style="display:none;">
          <label for="documentName" style="line-height: 27px;">
            <span style="font-size: 20px; color: green;">&#10003;</span>
            Documento abierto: </label>
            <input type="text" name="documentName" id="documentName" title="Change the file's name" style="margin-left: 5px;" value="" disabled="disabled" placeholder="...">
        </div>
        <div class="rp-section-content" style="line-height: 30px;">
        
      </div>
    </section>

    <section id="collabSections" class="disabled">
      <div class="notesBox rp-greyRuledTop rp-greyRuledBottom">
        <h3>
          <span>Edita</span><span style="color: #777; margin-left: 10px;">Colabora</span>
        </h3>
      </div>

      <section id="collabIntro" class="rp-greyRuledBottom" style="padding-bottom: 10px">
        <div class="rp-section-content">
          <div id="realtimeInitialized" style="display:none;margin-bottom:10px;">
            <span style="font-size: 20px; color: green;">&#10003;</span>
            Realtime API esta listo!
          </div>
        </div>
      </section>

      <section id="collabStringDemo" class="rp-greyRuledBottom" style="padding-bottom: 20px">
        <div class="rp-section-content">
          <h4>Caja de edicion</h4>
          <form>
          <textarea id="demoStringInput" name="demoStringInput" style="margin-top: 10px"></textarea>
          </form>
        </div>
      </section>
      
    </section>

<table id="demoLog" class="gmail knurling"></table>
  </div>
 <textarea id="code" name="code" style="margin-top: 10px"></textarea>
</body>

</html>
