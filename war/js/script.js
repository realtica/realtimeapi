(function() {
  var APPID, CLIENTID, initializeModel, loadModel, openCallback, pos2str, synchronize, _client, _editor, _markdown,_doc;

  APPID = "389448191089";

  CLIENTID = "389448191089.apps.googleusercontent.com";

  _client = null;

  _editor = null;

  $(function() {
    _editor = CodeMirror(document.getElementById("editor"), {
      mode: "text/html",
      lineNumbers: true,
      lineWrapping: true,
      readOnly: true
    });
    _doc=_editor.getDoc();
    _editor.setValue("");
    _editor.setSize("100%", "400");
    _client = new rtclient.RealtimeLoader({
      appId: APPID,
      clientId: CLIENTID,
      authButtonElementId: 'btn-auth',
      autoCreate: false,
      defaultTitle: "Sin titulo",
      initializeModel: initializeModel,
      onFileLoaded: loadModel
    });
    $('#btn-create').click(function() {
      if ($('#btn-create').hasClass('disabled')) {
        return;
      }
      $('#btn-create').addClass('disabled');
      $('#btn-open').addClass('disabled');
      $('#btn-share').addClass('disabled');
      return _client.createNewFileAndRedirect();
    });
    $('#btn-open').click(function() {
      if ($('#btn-open').hasClass('disabled')) {
        return;
      }
      $('#btn-auth').addClass('disabled');
      $('#btn-create').addClass('disabled');
      $('#btn-open').addClass('disabled');
      $('#btn-share').addClass('disabled');
      return google.load('picker', '1', {
        callback: function() {
          var picker, token, view;
          token = gapi.auth.getToken().access_token;
          view = new google.picker.View(google.picker.ViewId.DOCS);
          view.setMimeTypes("application/vnd.google-apps.drive-sdk." + APPID);
          picker = new google.picker.PickerBuilder().enableFeature(google.picker.Feature.NAV_HIDDEN).setAppId(APPID).setOAuthToken(token).addView(view).addView(new google.picker.DocsUploadView()).setCallback(openCallback).build();
          return picker.setVisible(true);
        }
      });
    });
    $('#btn-share').click(function() {
      if ($('#btn-share').hasClass('disabled')) {
        return;
      }
      var shareClient = new gapi.drive.share.ShareClient(APPID);
      shareClient.setItemIds([rtclient.params['fileId']]);
      shareClient.showSettingsDialog();
      //return alert("Share doesn't work without HTTPS, just used drive.google.com to change share settings!");
    });
    $('#btn-auth').removeClass('disabled');
    return _client.start(function() {
      $('#btn-auth').addClass('disabled');
      $('#btn-create').removeClass('disabled');
      return $('#btn-open').removeClass('disabled');
    });
  });

  openCallback = function(data) {
    var fileId;
    if (data.action === google.picker.Action.PICKED) {
      fileId = data.docs[0].id;
      return rtclient.redirectTo(fileId, _client.authorizer.userId);
    }
  };

  initializeModel = function(model) {
    var markdown;
    markdown = model.createString('<html><head></head><body></body></html>');
    return model.getRoot().set('markdown', markdown);
  };

  _markdown = null;

  loadModel = function(doc) {
    gapi.client.load('drive', 'v2', function() {
      var request;
      request = gapi.client.drive.files.get({
        fileId: rtclient.params['fileId']
      });
      $('#doc-name').attr('disabled', '');
      return request.execute(function(resp) {
        $('#doc-name').val(resp.title);
        $('#doc-name').removeAttr('disabled');
        return $('#doc-name').change(function() {
          var renameRequest;
          $('#doc-name').attr('disabled', '');
          renameRequest = gapi.client.drive.files.patch({
            fileId: rtclient.params['fileId'],
            resource: {
              title: $('#doc-name').val()
            }
          });
          return renameRequest.execute(function(resp) {
            $('#doc-name').val(resp.title);
            return $('#doc-name').removeAttr('disabled');
          });
        });
      });
    });
    $('#btn-share').removeClass('disabled');
    _markdown = doc.getModel().getRoot().get('markdown');
    _editor.setOption('readOnly', false);
    _editor.setValue(_markdown.getText());
    return synchronize(_editor, _markdown);
  };

  pos2str = function(_arg) {
    var ch, line;
    line = _arg.line, ch = _arg.ch;
    return line + ":" + ch;
  };

  synchronize = function(editor, markdown) {
    var ignore_change;
    ignore_change = false;
    editor.on('beforeChange', function(editor, changeObj) {
      var from, text, to;
      if (ignore_change) {
        return;
      }
      from = editor.indexFromPos(changeObj.from);
      to = editor.indexFromPos(changeObj.to);
      text = changeObj.text.join('\n');
      $("#preview").html(_doc.getValue());
      if (to - from > 0) {
        console.log("markdown.removeRange(" + from + ", " + to + ")");
        console.log(preview);
        markdown.removeRange(from, to);
      }
      if (text.length > 0) {
        console.log("markdown.insertString(" + from + ", '" + text + "')");
        console.log(preview);
        return markdown.insertString(from, text);
      }
      
    });
    markdown.addEventListener(gapi.drive.realtime.EventType.TEXT_INSERTED, function(e) {
      var from;
      if (e.isLocal) {
        return;
      }
      from = editor.posFromIndex(e.index);
      ignore_change = true;
      console.log("editor.replaceRange('" + e.text + "', " + (pos2str(from)) + ", " + (pos2str(from)) + ")");
      editor.replaceRange(e.text, from, from);
      return ignore_change = false;
    });
    return markdown.addEventListener(gapi.drive.realtime.EventType.TEXT_DELETED, function(e) {
      var from, to;
      if (e.isLocal) {
        return;
      }
      from = editor.posFromIndex(e.index);
      to = editor.posFromIndex(e.index + e.text.length);
      ignore_change = true;
      console.log("editor.replaceRange('', " + (pos2str(from)) + ", " + (pos2str(to)) + ")");
      editor.replaceRange("", from, to);
      return ignore_change = false;
    });
  };

}).call(this);